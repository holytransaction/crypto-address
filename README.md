# Crypto Address

## Installation

Add this line to your application's Gemfile:

    gem "crypto-address"

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install crypto-address

## Usage 

    # will validate as BTC address by default
    CryptoAddress.valid?('1FSG15yz41F3iH9pr6tQFfPrynVuyUz6KC')
    
    # for reular and multisig addresses
    CryptoAddress.valid?('DRmN5MHN1PDbiVHb7LufFVbsT1xn6ogvqH', :DOGE) 
    
    # testnet
    CryptoAddress.valid?('D5eL9T9fHNLxtAA1Pzcse88wPxLqvVmSdf', :DOGE_TEST)
    CryptoAddress.valid?('moEYD8dT52tF2KLTv9RQm2YFfQjrwe4Vse', :BTC_TEST)
    
    # will return false
    CryptoAddress.valid?('adsfasdfadsf', :BTC_TEST) 
    
    # will throw an exception
    CryptoAddress.valid?('adsfasdfadsf', :UNKNOWN_TYPE)

## Other languages

JavaScript: https://github.com/ryanralph/altcoin-address 
