require 'spec_helper'

describe CryptoAddress do
  describe "valid?" do
    it "should return false if address is valid but type is not matching to what specified" do
      CryptoAddress.valid?('BGYvHnVNezSJyzopYvJdXveSzVJkNUwjGt', :BTC).should == false
    end

    it "should return false if address is invalid" do
      CryptoAddress.valid?('3P1TEdL5DJYeRTSxYsaooaaaaaaaaaaaaa', :BTC).should == false
    end

    it "should return true if address is valid" do
      CryptoAddress.valid?('BGYvHnVNezSJyzopYvJdXveSzVJkNUwjGt', :BC).should == true
    end

    it "should throw exception if unknown address type is specified" do
      expect {
        CryptoAddress.valid?('someunkonwnaddress', :WTF)
      }.to raise_error
    end
  end
end