module CryptoAddress
  class EthereumValidator
    # CryptoAddress.valid?('0xb38483cb7a922694eb61a77c83aa1ec5eca20908', :ETH)
    ADDRESS_TEMPLATE = Regexp.new('0x(\d|[a-zA-Z]){40}')
    # address example '0xb38483cb7a922694eb61a77c83aa1ec5eca20908'
    class << self
      def valid?(address, type)
        !!((address.size == 42) && ADDRESS_TEMPLATE.match(address))
      end
    end
  end
end