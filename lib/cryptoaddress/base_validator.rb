module CryptoAddress
  class BaseValidator

    ADDRESS_TYPES = {
        BTC:  [0, 5],
        MP31:  [0, 5],
        LTC:  [48, 5],
        DOGE: [30, 22],
        PPC:  [55, 117],
        DASH:  [76, 05],
        BC:   [25, 85],
        RBR:  [61, 123],
        ETH:  [],
        GRC:  [62, 85],

        BTC_TEST:  [111, 196],
        MP31_TEST:  [111, 196],
        LTC_TEST:  [111, 196],
        DOGE_TEST: [113, 196],
        PPC_TEST:  [111, 196],
        DASH_TEST:  [111, 196],
        BC_TEST:   [111, 196],
        RBR_TEST:  [65, 127]
    }


    class << self

      def valid?(address, type = :BTC)
        raise CryptoAddress::Error, "Unknown currency" unless ADDRESS_TYPES.has_key?(type)
        begin
          hex = decode_base58(address)
        rescue Exception => e
          Rails.logger.error("== CryptoAddress ERROR ==\n#{ e.message }\n== BACKTRACE ==\n#{ e.backtrace }")
          false
        end
        return false unless hex && hex.bytesize == 50
        return false unless ADDRESS_TYPES[type].include?(hex[0...2].to_i(16))
        checksum_valid?(hex)
      end

      def decode_base58(base58_val)
        s = base58_to_int(base58_val).to_s(16); s = (s.bytesize.odd? ? '0'+s : s)
        s = '' if s == '00'
        leading_zero_bytes = (base58_val.match(/^([1]+)/) ? $1 : '').size
        s = ("00"*leading_zero_bytes) + s  if leading_zero_bytes > 0
        s
      end

      def base58_to_int(base58_val)
        alpha = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
        int_val, base = 0, alpha.size
        base58_val.reverse.each_char.with_index do |char, index|
          raise ArgumentError, 'Value not a valid Base58 String.' unless char_index = alpha.index(char)
          int_val += char_index*(base**index)
        end
        int_val
      end

      def checksum_valid?(hex)
        checksum( hex[0...42] ) == hex[-8..-1]
      end

      def checksum(hex)
        b = [hex].pack("H*") # unpack hex
        Digest::SHA256.hexdigest( Digest::SHA256.digest(b) )[0...8]
      end

    end

  end
end
