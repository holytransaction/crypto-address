require 'cryptoaddress/version'
require 'digest'

module CryptoAddress
  autoload :Error,              'cryptoaddress/error'
  autoload :BaseValidator,      'cryptoaddress/base_validator'
  autoload :EthereumValidator,  'cryptoaddress/ethereum_validator'

  TYPES = {
    ETH: EthereumValidator,
  }


  class << self
    def valid?(address, type = :BTC)
      validator(type).valid?(address, type)
    end

    private

    def validator(type)
      TYPES[type] || BaseValidator
    end
  end
end

