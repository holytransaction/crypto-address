# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'cryptoaddress/version'

Gem::Specification.new do |spec|
  spec.name          = "crypto-address"
  spec.version       = CryptoAddress::VERSION
  spec.authors       = ["nixoid"]
  spec.email         = ["nixoid@noveltylab.com"]
  spec.description   = %q{Universal cryptocurrency address validator}
  spec.summary       = %q{Read description}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec", "~> 2.6"
end
